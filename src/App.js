import React, {useState} from 'react'
import Header from './Header'
import ToDoList from './ToDoList'
import ToDoForm from './ToDoForm'
import './App.css';

export default function App() {
  const [toDoList, setToDoList] = useState([{}])

  const handleToggle = (id) => {
    let mapped = toDoList.map(task => {
      return id === task.id ? {...task,complete: !task.complete}: {...task};
    });
    setToDoList(mapped);
  }

  const handleFilter = () => {
    let filtered = toDoList.filter(task => {
      return !task.complete;
    });
    setToDoList(filtered)
  }

  const addTask = (userInput) => {
    let copy = [...toDoList];
    copy = [...copy, { id: toDoList.length + 1, task: userInput, complete: false}]
    setToDoList(copy);
  }

  return (
    <div className="App">
      <Header />
      <ToDoList toDoList = {toDoList} handleToggle={handleToggle} />
      <ToDoForm addTask={addTask} />
      <button onClick={handleFilter}>Delete Completed</button>
    </div>
  )
}

