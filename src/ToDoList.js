import React from 'react'
import ToDo from './ToDo'

const ToDoList = ({toDoList,handleToggle}) => {
    return (
        <div>
            {toDoList.map((todo,index) => {
                return (
                    <ToDo todo={todo} key={index} handleToggle={handleToggle} />
                )
            })}
        </div>
    )
}

export default ToDoList;
