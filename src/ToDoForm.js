import React,{ useState } from "react";

const ToDoForm = ({addTask}) => {
    const [userInput, setuserInput] = useState('');

    const handleChange = (e) => {
        setuserInput(e.currentTarget.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        addTask(userInput);
        setuserInput('');
    }

    return (
        <div>
            <form onSubmit={handleSubmit} >
                <input type="text" value={userInput} placeholder="Enter Task..." onChange={handleChange} />
                <button>Add Task</button>
            </form>
        </div>
    );
}

export default ToDoForm;
